#!/bin/bash
sudo tcpreplay --topspeed --intf1=eth1 --loop=10 --enable-file-cache --quiet floodfile1000_1.pcap &
sudo tcpreplay --topspeed --intf1=eth1 --loop=10 --enable-file-cache --quiet floodfile1000_2.pcap &
sudo tcpreplay --topspeed --intf1=eth1 --loop=10 --enable-file-cache --quiet floodfile1000_3.pcap &
sudo tcpreplay --topspeed --intf1=eth1 --loop=10 --enable-file-cache --quiet floodfile1000_4.pcap &
