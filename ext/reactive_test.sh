#!/bin/bash
GENERATOR="10.15.254.149"
SWBRAS="10.15.254.148"
USR="mininet"
USR2="root"
OUT="out.txt"
echo "Start tests in reactive mode"
ssh -t root@$GENERATOR '
cd /home/mininet/pox
nohup python pox.py --unthreaded_sh log.level --INFO transit_l4_hw openflow.of_01 --address=10.15.254.149 &
disown -h %1
'
