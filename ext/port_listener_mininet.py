# -*- coding: utf-8 -*-
#!/usr/bin/env python
import socket
import subprocess
import sys
from datetime import datetime
import time
import threading

# Clear the screen
subprocess.call('clear', shell=True)

num = raw_input("Enter number of ports to listen:")
numpack = raw_input("Enter number of packets to receive:")
num_ports  = int(num)
num_pack = int(numpack)

print "-" * 60
print "Opening Sockets for listening"
print "-" * 60

host = socket.gethostbyname('10.0.0.2')

global count
count = 0
lock = threading.Lock()

print '%.6f' %time.time()

class WorkerThread(threading.Thread):

  def __init__(self, i, num_pack, host):
    super(WorkerThread, self).__init__()
    self.port = i
    self.num_pack = num_pack
    self.host = host
    self.counter = 0
    self.data =''

  def run(self):

    try:
      global count
      sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      sock.settimeout(15)
      sock.bind((self.host, self.port))
      with lock:
        print 'port opened:', self.port
      while self.counter < self.num_pack:
        self.data, self.addr = sock.recvfrom(1024)
        if self.data != '':
          self.counter += 1
          with lock:
#            print '%.6f' %time.time(), 'from:', self.addr, 'packet_num:', self.counter
            count+=1
      sock.close()

    except socket.timeout:
        print "*",

    except KeyboardInterrupt:
      print "You pressed Ctrl+C"
      self.join()

  def join(self, timeout=None):
    super(WorkerThread, self).join()

try:

  pool= [WorkerThread(i, num_pack, host) for i in range(10000,10000+num_ports)]
  for thread in pool:
    thread.start()

except KeyboardInterrupt:
  print "You pressed Ctrl+C"
  sys.exit()

print "waitng threads..."
for thread in pool:
  thread.join()

print '%.6f' %time.time()

print 'job completed, count=', count

