# -*- coding: utf-8 -*-
#!/usr/bin/env python
import socket
import subprocess
import sys
from datetime import datetime
import time
import threading, Queue

num_packets = 1000000

src_addr = "\x00\x9C\x02\xAD\x4E\x1E"
dst_addr = "\x68\x05\xCA\x10\xA4\xB5"
payload = ("1"*44)
checksum = "\x1a\x2b\x3c\x4d"
ethertype = "\x08\x01"


try:
  t1 = time.time()
  sock = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
  sock.bind(('eth1',0))
  for i in range (0, num_packets):
    sock.send(dst_addr+src_addr+ethertype+payload+checksum)
  t2 = time.time()
  sock.close()

except socket.gaierror:
  print 'Hostname could not be resolved. Exiting'
  sys.exit()

except socket.error:
  print "Couldn't connect to server"
  sys.exit()

except KeyboardInterrupt:
  print "You pressed Ctrl+C"
  sys.exit()

deltat = t2 - t1
print 'Scanning Completed in:',deltat