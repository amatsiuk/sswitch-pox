# -*- coding: utf-8 -*-
from pox.core import core
from pox.datapaths.switch import SoftwareSwitchBase
from pox.datapaths import OpenFlowWorker
from pox.lib.util import str_to_dpid
from pox.lib.revent import EventMixin
from Queue import Queue
import threading
from pox.lib.revent import Event
import pox.lib.ioworker
import pox.openflow.libopenflow_01 as of
from copy import copy
import time
log = core.getLogger()

#Constants for Proxy:
FTEIR = 0.003 #Defines FTEIR limit for HwSw, seconds
prsw_address = '192.168.168.1'#loopback address Controller<->Proxy
hwsw_dpid = 3 #DPID of HWSW
swsw_dpid = 2 #DPID of SWSW

class Started (Event):
  def __init__ (self):
    Event.__init__(self)

class ModuleStarted (EventMixin):
  _eventMixin_events = set([
    Started,
  ])

waiting_controller = ModuleStarted()
waiting_switch =  ModuleStarted()

q_packetin = Queue()
q_flowmod_sw = Queue()
q_flowmod_hw = Queue()
q_deferred = Queue()
q_packetout = Queue()


class deferredWorker (threading.Thread):

  def __init__(self):
    super(deferredWorker , self).__init__()
    self.startevent = threading.Event()
    self.stopevent = threading.Event()

  def run (self):
    while core.running:
      ofp_def = q_deferred.get()
      ofp_def.buffer_id = None
      q_flowmod_hw.put(ofp_def)
      proxyctrl_hw.flow_mod_send()
      q_deferred.task_done()
      time.sleep(FTEIR)

  def join (self, timeout=None):
    self.stopevent.set()
    super(deferredWorker, self).join(timeout)


class ProxyControllerSw (object):
# Interacts with SwSw, parses PacketIn messages and places them into q_packetin
  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    log.info("Controlling SW_Switch dpid=%i" %(int(self.connection.dpid)))
    q_flowmod_sw.queue.clear()

  def _handle_PacketIn (self, event):
    packet = event.parsed
    if not packet.parsed:
      log.warning("Ignoring incomplete packet")
      return
    packet_in = event.ofp
    if packet.next.id == 10:
      packet_in.in_port = 3
    elif packet.next.id == 20:
      packet_in.in_port = 4
    packet.next.id = 1
    packet_in.data = packet
    q_packetin.put(packet_in)
    proxysw.packet_in_send()

  def flow_mod_send (self):
    msg=q_flowmod_sw.get()
    self.connection.send(msg)
    q_flowmod_sw.task_done()

  def packet_out_send (self):
    msg=q_packetout.get()
    self.connection.send(msg)
    q_packetout.task_done()


class ProxyControllerHw (ProxyControllerSw):

  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    log.info("Controlling HW_Switch dpid=%i" %(int(self.connection.dpid)))
    self.rules_installer()
    q_flowmod_hw.queue.clear()

  def _handle_PacketIn (self, event):
    log.warn('!!! Packet_In from HW_Switch')

  def flow_mod_send (self):
    msg = q_flowmod_hw.get()
    self.connection.send(msg)
    q_flowmod_hw.task_done()

  def rules_installer (self):
# Installs static FTEs to HwSw for offloading with prio=1000
    msg0 = of.ofp_flow_mod(command=of.OFPFC_DELETE)
    self.connection.send(msg0)

    msg1 = of.ofp_flow_mod()
    msg1.match.in_port = 3
    msg1.idle_timeout = 0
    msg1.hard_timeout = 0
    msg1.priority = 1000
    msg1.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
    msg1.actions.append(of.ofp_action_output(port = 47))
    self.connection.send(msg1)

    msg2 = of.ofp_flow_mod()
    msg2.match.in_port = 4
    msg2.idle_timeout = 0
    msg2.hard_timeout = 0
    msg2.priority = 1000
    msg2.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
    msg2.actions.append(of.ofp_action_output(port = 47))
    self.connection.send(msg2)

    msg3 = of.ofp_flow_mod()
    msg3.match.in_port = 48
    msg3.match.dl_vlan = 10
    msg3.idle_timeout = 0
    msg3.hard_timeout = 0
    msg3.priority = 1000
    msg3.actions.append(of.ofp_action_strip_vlan())
    msg3.actions.append(of.ofp_action_output(port = 3))
    self.connection.send(msg3)

    msg4 = of.ofp_flow_mod()
    msg4.match.in_port = 48
    msg4.match.dl_vlan = 20
    msg4.idle_timeout = 0
    msg4.hard_timeout = 0
    msg4.priority = 1000
    msg4.actions.append(of.ofp_action_strip_vlan())
    msg4.actions.append(of.ofp_action_output(port =4))
    self.connection.send(msg4)

    msg5 = of.ofp_flow_mod()
    msg5.match.in_port = 48
    msg5.match.dl_vlan = 110
    msg5.idle_timeout = 0
    msg5.hard_timeout = 0
    msg5.priority = 1000
    msg5.actions.append(of.ofp_action_strip_vlan())
    msg5.actions.append(of.ofp_action_output(port = 4))
    self.connection.send(msg5)

    msg6 = of.ofp_flow_mod()
    msg6.match.in_port = 48
    msg6.match.dl_vlan = 120
    msg6.idle_timeout = 0
    msg6.hard_timeout = 0
    msg6.priority = 1000
    msg6.actions.append(of.ofp_action_strip_vlan())
    msg6.actions.append(of.ofp_action_output(port = 3))
    self.connection.send(msg6)


class ProxySwitch(SoftwareSwitchBase):
# Interacts with Controller
  def __init__(self,address, port, max_retry_delay, *args, **kw):
    self.loop = pox.lib.ioworker.RecocoIOLoop()
    self.address = address
    self.port = port
    self.max_retry_delay = max_retry_delay
    self.loop.start()
    super(ProxySwitch, self).__init__(*args,**kw)
    self.ofworker = OpenFlowWorker.begin(loop=self.loop, addr=self.address,
       port=self.port, max_retry_delay=self.max_retry_delay, switch=self)
    self.deferredworker = deferredWorker()
    self.tick = time.time()
    self.delta_t = self.tick
    q_packetin.queue.clear()
    q_deferred.queue.clear()
    self.deferredworker.start()

  def packet_in_send (self):
    msg=q_packetin.get()
    self.send(msg)
    q_packetin.task_done()

  def _rx_packet_out (self, packet_out, connection):
    ruleextr = packet_out.actions.pop()
    if (packet_out.in_port == 3) and (ruleextr.port == 4):
      packet_out.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
    elif (packet_out.in_port == 4) and (ruleextr.port == 3):
      packet_out.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
    elif (packet_out.in_port == 3) and (ruleextr.port == of.OFPP_FLOOD):
      packet_out.actions.append(of.ofp_action_vlan_vid(vlan_vid = 110))
    elif (packet_out.in_port == 4) and (ruleextr.port == of.OFPP_FLOOD):
      packet_out.actions.append(of.ofp_action_vlan_vid(vlan_vid = 120))
    packet_out.actions.append(of.ofp_action_output(port = 2))
    q_packetout.put(packet_out)
    proxyctrl_sw.packet_out_send()

  def _rx_flow_mod (self, ofp, connection):
    self.delta_t = time.time()-self.tick
    self.tick = time.time()
    if (self.delta_t > FTEIR) and (ofp.command != 3):
      buf_id = copy(ofp.buffer_id)
      ofp.buffer_id = None
      q_flowmod_hw.put(ofp)
      proxyctrl_hw.flow_mod_send()
# Packet_out to SwSw instead of Flow-mod because packet caused Packet-in
#still resides in the Buffer of SwSw:
      msg = of.ofp_packet_out()
      msg.buffer_id = buf_id
      if ofp.actions[0].port == 3:
        msg.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
      elif ofp.actions[0].port == 4:
        msg.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
      msg.actions.append(of.ofp_action_output(port = 2))
      msg.in_port = 1
      q_packetout.put(msg)
      proxyctrl_sw.packet_out_send()
#Alternatevely Flow-mod can be sent to SwSw uncomment following 10 lines
#and comment out 10 lines above
      #ruleextr = ofp.actions.pop()
      #if ruleextr.port == 1:
        #ofp.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
      #elif ruleextr.port == 2:
        #ofp.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
      #ofp.actions.append(of.ofp_action_output(port = 2))
      #ofp.match.in_port = 1
      #ofp.buffer_id = buf_id
      #q_flowmod_sw.put(ofp)
      #proxyctrl_sw.flow_mod_send()
    elif ofp.command == 3: #resend clear flows command to SwSw
      q_flowmod_sw.put(ofp)
      proxyctrl_sw.flow_mod_send()
    else:
      ofp_out_act = copy(ofp.actions)
      ofp_out_match = copy(ofp.match)
      ruleextr = ofp.actions.pop()
      if ruleextr.port == 3:
        ofp.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
      elif ruleextr.port == 4:
        ofp.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
      ofp.actions.append(of.ofp_action_output(port = 2))
      ofp.match.in_port = 1
      q_flowmod_sw.put(ofp)
      proxyctrl_sw.flow_mod_send()
      ofp.actions = ofp_out_act
      ofp.match = ofp_out_match
      q_deferred.put(ofp)


#launching ProxySwitch instance to a Controller, define address and port
def proxyswitch (address=prsw_address, port = 6633, max_retry_delay = 16,
    dpid ='7', extra = None, __INSTANCE__ = None):
  dpid = str_to_dpid(dpid)
  port = int(port)
  max_retry_delay = int(max_retry_delay)

  def start_proxyswitch (event):
    def handle_SwitchStarted (event):
      global proxysw
      proxysw = ProxySwitch(dpid = dpid, name="sw"+str(dpid),
      address = address, port = port, max_retry_delay = max_retry_delay)
    log.info("Waiting for connection to switch...")
    waiting_switch.addListenerByName("Started", handle_SwitchStarted)
  core.addListenerByName("UpEvent", start_proxyswitch)

#launching ProxyControllers instances to both switches
def proxycontroller (address='0.0.0.0', port = 6633, __INSTANCE__ = None):
  def start_proxycontroller (event):
    if event.dpid == hwsw_dpid:#Initializing ProxyController for HwSw
      global proxyctrl_hw
      proxyctrl_hw = ProxyControllerHw(event.connection)
    elif event.dpid == swsw_dpid:#Initializing ProxyController for SwSw
      global proxyctrl_sw
      proxyctrl_sw = ProxyControllerSw(event.connection)
      waiting_switch.raiseEvent(Started)
  core.openflow.addListenerByName("ConnectionUp", start_proxycontroller)
