#!/usr/bin/env python
from time import sleep
import subprocess
from subprocess import Popen
import logging
import pexpect
from pxssh import pxssh, ExceptionPxssh

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger('tester')

server2='10.15.254.148'
username2='root'
password2='swbras'
copier = pexpect.spawn('scp %s@%s:/var/ramfs/*.png .' %(username2, server2))
#copier.logfile = open("script.log","a+")
copier.expect('password:')
logging.info('sending password')
copier.sendline (password2)
copier.expect(pexpect.EOF)
logging.info('copy completed')