from pox.core import core
import pox.openflow.libopenflow_01 as of

#Test L4 controller with reduced functionality and faster reaction;
#doesn't analyze incoming port,Sends all packets to port 2 of SwSw,
#changes udp.dstport in consequent FTEs


log = core.getLogger()

#Constants:
hwsw_dpid = 3 #DPID of HwSw
swsw_dpid = 2 #DPID of SwSw

class TestControllerHw (object):

  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    self.rules_installer()

  def rules_installer (self):
# Method for installation of static flows to a Hw_Switch
    msg0 = of.ofp_flow_mod(command=of.OFPFC_DELETE)
    self.connection.send(msg0)

    msg1 = of.ofp_flow_mod()
    msg1.match.in_port = 3
    msg1.idle_timeout = 0
    msg1.hard_timeout = 0
    msg1.priority = 1000
    msg1.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
    msg1.actions.append(of.ofp_action_output(port = 47))
    self.connection.send(msg1)

    msg2 = of.ofp_flow_mod()
    msg2.match.in_port = 4
    msg2.idle_timeout = 0
    msg2.hard_timeout = 0
    msg2.priority = 1000
    msg2.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
    msg2.actions.append(of.ofp_action_output(port = 47))
    self.connection.send(msg2)

    msg3 = of.ofp_flow_mod()
    msg3.match.in_port = 48
    msg3.match.dl_vlan = 10
    msg3.idle_timeout = 0
    msg3.hard_timeout = 0
    msg3.priority = 1000
    msg3.actions.append(of.ofp_action_strip_vlan())
    msg3.actions.append(of.ofp_action_output(port = 3))
    self.connection.send(msg3)

    msg4 = of.ofp_flow_mod()
    msg4.match.in_port = 48
    msg4.match.dl_vlan = 20
    msg4.idle_timeout = 0
    msg4.hard_timeout = 0
    msg4.priority = 1000
    msg4.actions.append(of.ofp_action_strip_vlan())
    msg4.actions.append(of.ofp_action_output(port =4))
    self.connection.send(msg4)
    log.info("Hw static rules installed")

class TestController (object):

  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    self.dstport_to_port = {}
    self.out_port = 2
    self.msg = of.ofp_flow_mod()
    self.msg.priority = 25000
    self.msg.match.in_port = 1
    self.msg.match.dl_type = 0x0800
    self.msg.match.nw_proto = 17
    self.msg.idle_timeout = 0
    self.msg.hard_timeout = 360
    self.msg.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
    self.msg.actions.append(of.ofp_action_output(port = self.out_port))

  def act_like_switch (self, packet, packet_in):
    dstport = packet.next.next.next.dstport
    if dstport not in self.dstport_to_port:
      self.dstport_to_port[dstport] = self.out_port
      self.msg.match.tp_dst = dstport
      self.connection.send(self.msg)
    else:
        pass  #Ignore consequent packets of the same flow

  def _handle_PacketIn (self, event):
    packet = event.parsed
    packet_in = event.ofp
    self.act_like_switch(packet, packet_in)


def launch ():
  def start_switch (event):
    if event.dpid == hwsw_dpid:
      log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
      global testcontroller_hw
      testcontroller_hw = TestControllerHw(event.connection)
    elif event.dpid == swsw_dpid:
      log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
      global testcontroller
      testcontroller = TestController(event.connection)
    else:
     log.info("Ignore conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
  core.openflow.addListenerByName("ConnectionUp", start_switch)