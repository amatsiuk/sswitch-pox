from pox.core import core
import pox.openflow.libopenflow_01 as of
import time

#Static L4 rules with HwSw only: 3:udpport -> 4:udpport,
log = core.getLogger()

#Constants:
hwsw_dpid = 3 #DPID of HWSW
num_flows = 10000 #number of static flows starting udp.dstport = 10,000

class TestController (object):
  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    self.act_like_switch()

  def act_like_switch (self):
    msg = of.ofp_flow_mod()
    msg.match.in_port = 3
    msg.idle_timeout = 0
    msg.hard_timeout = 0
    msg.match.dl_type = 0x0800
    msg.match.nw_proto = 17
    msg.priority = 25000
    msg.actions.append(of.ofp_action_output(port = 4))
    for i in range (10000, 10000+num_flows):
      msg.match.tp_dst = i
      self.connection.send(msg)
#      log.info("Flow_Mod: 3 -> 4:%i" %i)
    self.connection.send(of.ofp_barrier_request(xid = msg.xid))
    self.t2 = time.time()

  def _handle_BarrierIn (self, event):
    deltat2 = time.time() - self.t2
    log.info("Barrier Reply xid= %i returned in: %.4f" %(event.xid, deltat2))

def launch ():
  def start_switch (event):
    if event.dpid == hwsw_dpid:
      log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
      global testcontroller
      testcontroller = TestController(event.connection)
    else:
     log.info("Ignore conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
  core.openflow.addListenerByName("ConnectionUp", start_switch)
