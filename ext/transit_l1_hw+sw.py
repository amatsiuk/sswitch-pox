from pox.core import core
import pox.openflow.libopenflow_01 as of

#Offloads all traffic from HwSw to SwSw: with VLAN Tag rewriting in SwSw
log = core.getLogger()

#Constants:
hwsw_dpid = 3 #DPID of HWSW
swsw_dpid = 2 #DPID of SWSW


class ProxyControllerHw (object):

  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    self.act_like_switch()


  def act_like_switch (self):
    msg1 = of.ofp_flow_mod()
    msg1.match.in_port = 3
    msg1.idle_timeout = 0
    msg1.hard_timeout = 0
    msg1.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
    msg1.actions.append(of.ofp_action_output(port = 47))
    self.connection.send(msg1)
    log.info("Flow_Mod: 3 -> 47")
    msg2 = of.ofp_flow_mod()
    msg2.match.in_port = 4
    msg2.idle_timeout = 0
    msg2.hard_timeout = 0
    msg2.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
    msg2.actions.append(of.ofp_action_output(port = 47))
    self.connection.send(msg2)
    log.info("Flow_Mod: 4 -> 47")
    msg3 = of.ofp_flow_mod()
    msg3.match.in_port = 48
    msg3.match.dl_vlan = 10
    msg3.idle_timeout = 0
    msg3.hard_timeout = 0
    msg3.actions.append(of.ofp_action_strip_vlan())
    msg3.actions.append(of.ofp_action_output(port = 3))
    self.connection.send(msg3)
    log.info("Flow_Mod: 48 -> 3")
    msg4 = of.ofp_flow_mod()
    msg4.match.in_port = 48
    msg4.match.dl_vlan = 20
    msg4.idle_timeout = 0
    msg4.hard_timeout = 0
    msg4.actions.append(of.ofp_action_strip_vlan())
    msg4.actions.append(of.ofp_action_output(port =4))
    self.connection.send(msg4)
    log.info("Flow_Mod: 48 -> 4")



class ProxyControllerSw (object):

  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    self.act_like_switch()

  def act_like_switch (self):
    msg1 = of.ofp_flow_mod()
    msg1.match.in_port = 1
    msg1.match.dl_vlan = 10
    msg1.idle_timeout = 0
    msg1.hard_timeout = 0
    msg1.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
    msg1.actions.append(of.ofp_action_output(port = 2))
    self.connection.send(msg1)
    log.info("Flow_Mod: 1:10 -> 2:20")
    msg2 = of.ofp_flow_mod()
    msg2.match.in_port = 1
    msg2.match.dl_vlan = 20
    msg2.idle_timeout = 0
    msg2.hard_timeout = 0
    msg2.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
    msg2.actions.append(of.ofp_action_output(port = 2))
    self.connection.send(msg2)
    log.info("Flow_Mod: 1:20 -> 2:10")


def launch ():

  def start_switch (event):
    if event.dpid == hwsw_dpid:#Initializing ProxyController for HwSw
      global proxyctrl_hw
      proxyctrl_hw = ProxyControllerHw(event.connection)
      log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
    elif event.dpid == swsw_dpid:#Initializing ProxyController for SwSw
      global proxyctrl_sw
      proxyctrl_sw = ProxyControllerSw(event.connection)
      log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
    else:
     log.info("Ignore conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
  core.openflow.addListenerByName("ConnectionUp", start_switch)
