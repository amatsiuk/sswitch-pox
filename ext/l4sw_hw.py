from pox.core import core
import pox.openflow.libopenflow_01 as of
import time 

#Test L4 controller with reduced functionality and faster reaction;
#doesn't analyze incoming port,Sends all packets to port 4 of HwSw,
#changes udp.dstport in consequent FTEs

log = core.getLogger()

#Constants:
hwsw_dpid = 7 #DPID of HWSW
hwsw_dpid2 = 1 #DPID of OVS

class TestController (object):

  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    self.dstport_to_port = {}
    self.out_port = 2
    self.msg = of.ofp_flow_mod()
    self.msg.priority = 25000
    self.msg.match.in_port = 1
    self.msg.match.dl_type = 0x0800
    self.msg.match.nw_proto = 17
    self.msg.idle_timeout = 0
    self.msg.hard_timeout = 360
    self.msg.actions.append(of.ofp_action_output(port = self.out_port))
    self.pcounter = 0
    self.bcounter = 0
    self.tick = time.time()

  def act_like_switch (self, packet, packet_in):
    dstport = packet.next.next.dstport
    if dstport not in self.dstport_to_port:
      self.dstport_to_port[dstport] = self.out_port
      self.msg.match.tp_dst = dstport
      self.connection.send(self.msg)
    else:
        pass #Ignore consequent packets of the same flow

  def _handle_PacketIn (self, event):
    self.pcounter += 1
    if self.pcounter % 100 == 0:
      self.tick = time.time()
      log.info("pcounter: %i time: %.6f" %(self.pcounter, self.tick))
    #packet = event.parsed
    #packet_in = event.ofp
    #self.act_like_switch(packet, packet_in)
    #pass
    
    
  def _handle_BarrierIn (self, event):
    self.bcounter += 1
    if self.bcounter % 100 == 0:
      log.info('bcounter: %i' %self.bcounter)

def launch ():
  def start_switch (event):
    if event.dpid == hwsw_dpid or event.dpid == hwsw_dpid2:
      log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
      global testcontroller
      testcontroller = TestController(event.connection)
    else:
     log.info("Ignore conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
  core.openflow.addListenerByName("ConnectionUp", start_switch)
