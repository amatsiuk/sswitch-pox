# -*- coding: utf-8 -*-
#!/usr/bin/env python
import socket
import subprocess
import sys
from datetime import datetime
import time
import threading, Queue


# Clear the screen
subprocess.call('clear', shell=True)

remoteServerIP  = socket.gethostbyname('10.0.0.2')

num = raw_input("Enter number of ports to scan:")
num_ports  = int(num)
num_bytes = 86
str_to_send = '1'*num_bytes
print "-" * 60
print "Please wait, scanning remote host", remoteServerIP
print "-" * 60
#adj_sleep = (((1000-num_ports)*0.000152)/num_ports) + (0.85/num_ports)

adj_sleep = 1.000000/num_ports - ((num_ports -200)*0.025)/(1000*num_ports)


if adj_sleep < 0:
  adj_sleep = 0.000001
  print 'interval force = 1ms'
else:
  print 'interval between packets= %.6f s' %adj_sleep

def hr_sleep(duration):
  end = time.time() + duration
#  if duration == 0.001:
#    time.sleep(duration)
  while time.time() - end < 0:
    time.sleep(0)

try:
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  for i in range(30000, 30000 + num_ports):
    sock.sendto(str_to_send, (remoteServerIP, i))
    if i == 30000:
      t1 = time.time()
    elif i == 30000 + num_ports - 1:
      t2 = time.time()
    hr_sleep(adj_sleep)
  sock.close()

except socket.gaierror:
  print 'Hostname could not be resolved. Exiting'
  sys.exit()

except socket.error:
  print "Couldn't connect to server"
  sys.exit()

except KeyboardInterrupt:
  print "You pressed Ctrl+C"
  sys.exit()

deltat = t2 - t1
print 'Scanning Completed in:',deltat
