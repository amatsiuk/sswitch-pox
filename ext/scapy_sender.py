#!/usr/bin/python
from scapy.all import *
import sys
from datetime import datetime
import time
from copy import copy,deepcopy
t1 = time.time()
try:
  packetlistsend = rdpcap("/home/mininet/scripts/floodfile800.pcap")
  print 'packet list created'
  sendpfast(packetlistsend, pps=1000000, iface='eth1',file_cache = True)
#  sendp(packet, iface='br0',verbose=False)

except KeyboardInterrupt:
  print "You pressed Ctrl+C"
  sys.exit()
t2 = time.time()
deltat = t2 - t1
print 'Creation Completed in:',deltat