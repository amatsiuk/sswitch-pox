from pox.core import core
import pox.openflow.libopenflow_01 as of

#Static L1 rules in HwSw only: Port 3->4, 4->3
log = core.getLogger()

#Constants:
hwsw_dpid = 3 #DPID of HWSW

class TestController (object):

  def __init__ (self, connection):

    self.connection = connection
    connection.addListeners(self)
    self.act_like_switch()

  def act_like_switch (self):
    msg = of.ofp_flow_mod()
    msg.match.in_port = 3
    msg.idle_timeout = 0
    msg.hard_timeout = 0
    msg.actions.append(of.ofp_action_output(port = 4))
    self.connection.send(msg)
    log.info("Flow_Mod: 3 -> 4")
    msg = of.ofp_flow_mod()
    msg.match.in_port = 4
    msg.idle_timeout = 0
    msg.hard_timeout = 0
    msg.actions.append(of.ofp_action_output(port = 3))
    self.connection.send(msg)
    log.info("Flow_Mod: 4 -> 3")


def launch ():

  def start_switch (event):
    if event.dpid == hwsw_dpid:
      log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
      global testcontroller
      testcontroller = TestController(event.connection)
    else:
     log.info("Ignore conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
  core.openflow.addListenerByName("ConnectionUp", start_switch)
