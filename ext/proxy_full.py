from pox.core import core
from pox.datapaths.switch import SoftwareSwitchBase
from pox.datapaths import OpenFlowWorker
from pox.lib.util import dpid_to_str, str_to_dpid
from pox.lib.revent import EventMixin
from pox.lib.packet.vlan import vlan
from pox.lib.packet.ethernet import ethernet
from Queue import Queue, Empty
import threading
from pox.lib.revent import Event
import pox.lib.ioworker
import pox.openflow.libopenflow_01 as of
from copy import deepcopy, copy
from collections import deque
import time
log = core.getLogger()


class Started (Event):
  def __init__ (self):
    Event.__init__(self)

class ModuleStarted (EventMixin):
  _eventMixin_events = set([
    Started,
  ])

waiting_controller = ModuleStarted()
waiting_switch =  ModuleStarted()

q_packetin = Queue()
q_flowmod_sw = Queue()
q_flowmod_hw = Queue()
q_deferred = Queue()
q_packetout = Queue()
q_aggflst_down_hw = Queue()
q_aggflst_down_sw = Queue()
q_aggflst_up_hw = Queue()
q_aggflst_up_sw = Queue()
q_flst_down_hw = Queue()
q_flst_down_sw = Queue()
q_flst_up_hw = Queue()
q_flst_up_sw = Queue()


class deferredWorker (threading.Thread):

  def __init__(self):
    super(deferredWorker , self).__init__()
    self.startevent = threading.Event()
    self.stopevent = threading.Event()
    self.counter = 0

  def run (self):
#    self.startevent.wait(timeout=3)
    while core.running:
      ofp_def = q_deferred.get()
      ofp_def.buffer_id = None
      match_flst = copy(ofp_def.match)
      match_flst.in_port = 1
#      ofp_def.actions.insert(0, of.ofp_action_strip_vlan()) #!
      q_flowmod_hw.put(ofp_def)
      self.counter += 1
      log.info('%i Thread-->HW_Switch' %(self.counter))
      ofp_def.hard_timeout = ofp_def.hard_timeout - \
      round(time.time() - ofp_def.timestamp)
      log.info('New hard_timeout: %i' %ofp_def.hard_timeout)
      proxyctrl_hw.flow_mod_send()
      flst = of.ofp_stats_request()
      flst.type = of.OFPST_FLOW
      flst.body = of.ofp_flow_stats_request()
      flst.body.match = match_flst
      q_flst_down_sw.put(flst)
      proxyctrl_sw.FlowStats_send()
      q_deferred.task_done()
      time.sleep(0.005) #Delay between consequent rules to Hw_Sw

  def join (self, timeout=None):
    self.stopevent.set()
    super(deferredWorker, self).join(timeout)


class ProxyControllerSw (object):
# Interacts with OFSwitch, parses PacketIn messages and
#places them into q_packetin
  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    log.info("Controlling SW_Switch dpid=%i" %(int(self.connection.dpid)))
    q_flowmod_sw.queue.clear()
    self.countermod = 0
    self.counterout = 0
    self.counterin = 0
    self.conterflowrem = 0


  def _handle_FlowRemoved (self, event):
    if not event.hardTimeout:
      return
    self.conterflowrem += 1
    log.info("%i Flow_Removed Sw_Sw!" %self.conterflowrem)

  def _handle_PacketIn (self, event):
    self.counterin += 1
    log.info('%i Sw_Sw-->Packet_In' %self.counterin)
    inport = event.port
    packet = event.parsed
    if not packet.parsed:
      log.warning("Ignoring incomplete packet")
      return
    packet_in = event.ofp
    if packet.next.id == 10:
      packet_in.in_port = 3
    elif packet.next.id == 20:
      packet_in.in_port = 4
    packet.next.id = 1
    packet_in.data = packet
    q_packetin.put(packet_in)
    proxysw.packet_in_send()

  def _handle_AggregateFlowStatsReceived (self, event):
    q_aggflst_up_sw.put(event)

  def _handle_FlowStatsReceived (self, event):
    q_flst_up_sw.put(event)
    proxysw.FlowStatsSw_save()

  def flow_mod_send (self):
    msg=q_flowmod_sw.get()
    self.connection.send(msg)
    q_flowmod_sw.task_done()
    self.countermod+=1
    log.info('%i Flow_Mod-->Sw_Switch' %self.countermod)

  def packet_out_send (self):
    msg=q_packetout.get()
    self.connection.send(msg)
    self.counterout+=1
    log.info('%i Packet_Out-->Sw_Switch' %self.counterout)
    q_packetout.task_done()

  def FlowStats_send (self):
    flst = q_flst_down_sw.get()
    self.connection.send(flst)
    q_flst_down_sw.task_done()

  def AggregateFlowStats_send (self):
    flst = q_aggflst_down_sw.get()
    self.connection.send(flst)
    q_aggflst_down_sw.task_done()


class ProxyControllerHw (ProxyControllerSw):

  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    log.info("Controlling HW_Switch dpid=%i" %(int(self.connection.dpid)))
    self.rules_installer()
    q_flowmod_hw.queue.clear()
    self.conterflowrem = 0

  def _handle_PacketIn (self, event):
    log.info('!!! Packet_In from HW_Switch')

  def _handle_BarrierIn (self, event):
    log.info('Barrier Reply xid= %i' %event.xid)

  def _handle_AggregateFlowStatsReceived (self, event):
    q_aggflst_up_hw.put(event)
    proxysw.AggregateFlowStats_send()


  def _handle_FlowStatsReceived (self, event):
    q_flst_up_hw.put(event)
    proxysw.FlowStatsHw_send()

  def _handle_FlowRemoved (self, event):
    if not event.hardTimeout:
      return
    self.conterflowrem += 1
    log.info("%i Flow_Removed Hw_Sw!" %self.conterflowrem)

  def FlowStats_send (self):
    flst = q_flst_down_hw.get()
    self.connection.send(flst)
    q_flst_down_hw.task_done()

  def AggregateFlowStats_send (self):
    flst = q_aggflst_down_hw.get()
    self.connection.send(flst)
    q_aggflst_down_hw.task_done()

  def flow_mod_send (self):
    msg = q_flowmod_hw.get()
    self.connection.send(msg)
    q_flowmod_hw.task_done()
#    self.connection.send(of.ofp_barrier_request(xid = msg.xid))


  def rules_installer (self):
# Method for installation of static flows to a Hw_Switch
    #HW_Switch table clearing
    msg0 = of.ofp_flow_mod(command=of.OFPFC_DELETE)
    self.connection.send(msg0)
    #del msg0
    #time.sleep(1)

    msg1 = of.ofp_flow_mod()
    msg1.match.in_port = 3
    msg1.idle_timeout = 0
    msg1.hard_timeout = 0
    msg1.priority = 1000
    msg1.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
    msg1.actions.append(of.ofp_action_output(port = 47))
    self.connection.send(msg1)

    msg2 = of.ofp_flow_mod()
    msg2.match.in_port = 4
    msg2.idle_timeout = 0
    msg2.hard_timeout = 0
    msg2.priority = 1000
    msg2.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
    msg2.actions.append(of.ofp_action_output(port = 47))
    self.connection.send(msg2)

    msg3 = of.ofp_flow_mod()
    msg3.match.in_port = 48
    msg3.match.dl_vlan = 10
    msg3.idle_timeout = 0
    msg3.hard_timeout = 0
    msg3.priority = 1000
    msg3.actions.append(of.ofp_action_strip_vlan())
    msg3.actions.append(of.ofp_action_output(port = 3))
    self.connection.send(msg3)

    msg4 = of.ofp_flow_mod()
    msg4.match.in_port = 48
    msg4.match.dl_vlan = 20
    msg4.idle_timeout = 0
    msg4.hard_timeout = 0
    msg4.priority = 1000
    msg4.actions.append(of.ofp_action_strip_vlan())
    msg4.actions.append(of.ofp_action_output(port =4))
    self.connection.send(msg4)

    msg5 = of.ofp_flow_mod()
    msg5.match.in_port = 48
    msg5.match.dl_vlan = 110
    msg5.idle_timeout = 0
    msg5.hard_timeout = 0
    msg5.priority = 1000
    msg5.actions.append(of.ofp_action_strip_vlan())
    msg5.actions.append(of.ofp_action_output(port = 4))
    self.connection.send(msg5)

    msg6 = of.ofp_flow_mod()
    msg6.match.in_port = 48
    msg6.match.dl_vlan = 120
    msg6.idle_timeout = 0
    msg6.hard_timeout = 0
    msg6.priority = 1000
    msg6.actions.append(of.ofp_action_strip_vlan())
    msg6.actions.append(of.ofp_action_output(port = 3))
    self.connection.send(msg6)


class ProxySwitch(SoftwareSwitchBase):
# Interacts with Controller
  def __init__(self,address, port, max_retry_delay, *args, **kw):
    self.loop = pox.lib.ioworker.RecocoIOLoop()
    self.address = address
    self.port = port
    self.max_retry_delay = max_retry_delay
    self.loop.start()
    super(ProxySwitch, self).__init__(*args,**kw)
    self.ofworker = OpenFlowWorker.begin(loop=self.loop, addr=self.address,
       port=self.port, max_retry_delay=self.max_retry_delay, switch=self)
    self.deferredworker = deferredWorker()
    self.tick = time.time()
    self.delta_t = self.tick
    q_packetin.queue.clear()
    q_deferred.queue.clear()
    q_aggflst_down_hw.queue.clear()
    q_aggflst_down_sw.queue.clear()
    q_aggflst_up_hw.queue.clear()
    q_aggflst_up_sw.queue.clear()
    q_flst_up_hw.queue.clear()
    q_flst_up_sw.queue.clear()
    self.deferredworker.start()
    self.counterdef = 0
    self.counterdir = 0
    self.dstport_to_stats= {}

  def packet_in_send (self):
    msg=q_packetin.get()
    self.send(msg)
    q_packetin.task_done()

  def FlowStatsHw_send (self):
    flst = q_flst_up_hw.get()
#    flst.ofp[0].flags = 0 # or alternatively resend all ofps in ofp[i]
# TO DO Implement branch where flow hasn't been moved to Hw_Sw yet'
#    if flst.ofp[0].flags == 0 && flst.ofp[0].xid == ?? Saved_from_SW_SW ??
#      get and sent stats from dictionary
    for ofp_st in flst.ofp:
      if ofp_st.body != []:
        log.info("Fl_St from Hw_Sw Packet_count:%i"
        %ofp_st.body[0].packet_count)
        if ofp_st.body[0].match.tp_dst in self.dstport_to_stats:
          log.info("Flow dst_port:%i already was in Sw_Sw"
          %ofp_st.body[0].match.tp_dst)
          ofp_st.body[0].packet_count += \
          self.dstport_to_stats[ofp_st.body[0].match.tp_dst].packet_count
          ofp_st.body[0].byte_count += \
          self.dstport_to_stats[ofp_st.body[0].match.tp_dst].byte_count
      self.send(ofp_st)
    q_flst_up_hw.task_done()

  def FlowStatsSw_save (self):
    flst = q_flst_up_sw.get()
    for st in flst.stats:
      if st != None:
        self.dstport_to_stats[st.match.tp_dst] = st
        print 'Stats saved, dstport=',st.match.tp_dst,'packets=',st.packet_count
    q_flst_up_sw.task_done()

  def AggregateFlowStats_send (self):
#Analytically for counters except flows:Nstats=Nhw-Nsw-Nsw_pout(?)-Ndrop_in_sw
    flst_hw = q_aggflst_up_hw.get()
    try:
      flst_sw = q_aggflst_up_sw.get_nowait()
    except Empty:
      log.info("No Aggregate Stats from Sw_Sw")
      q_aggflst_up_hw.task_done()
    else:
      flst_hw.ofp.body.byte_count -= flst_sw.stats.byte_count
      flst_hw.ofp.body.packet_count -= flst_sw.stats.packet_count
      q_aggflst_up_hw.task_done()
      q_aggflst_up_sw.task_done()
    flst_hw.ofp.body.flow_count -= 6 #decrease by number of static flows
    reply = of.ofp_stats_reply(xid=flst_hw.ofp.xid,
                            type=flst_hw.ofp.type,
                            body=flst_hw.ofp.body)
    self.send(reply)

  def _rx_stats_request (self, ofp, connection):
    if ofp.type == of.OFPST_AGGREGATE:
      log.info('Aggregate Flow stats request received')
      q_aggflst_down_hw.put(ofp)
      q_aggflst_down_sw.put(ofp)
      proxyctrl_hw.AggregateFlowStats_send()
      proxyctrl_sw.AggregateFlowStats_send()
    elif ofp.type == of.OFPST_FLOW:
      log.info('Flow stats request dst_port: %i received' %ofp.body.match.tp_dst)
      if ofp.body.match.tp_dst in self.dstport_to_stats:
        q_flst_down_hw.put(ofp)
        proxyctrl_hw.FlowStats_send()
      else:
        q_flst_down_hw.put(ofp)
        proxyctrl_hw.FlowStats_send()
        ofp.body.match.in_port = 1 #Match for Sw_Sw should be rewritten twice
        q_flst_down_sw.put(ofp)
        proxyctrl_sw.FlowStats_send()
    else:
      log.warning("Non-implemented stats type")

  def _rx_packet_out (self, packet_out, connection):
    ruleextr = packet_out.actions.pop()
    if (packet_out.in_port == 3) and (ruleextr.port == 4):
      packet_out.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
    elif (packet_out.in_port == 4) and (ruleextr.port == 3):
      packet_out.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
    elif (packet_out.in_port == 3) and (ruleextr.port == of.OFPP_FLOOD):
      packet_out.actions.append(of.ofp_action_vlan_vid(vlan_vid = 110))
    elif (packet_out.in_port == 4) and (ruleextr.port == of.OFPP_FLOOD):
      packet_out.actions.append(of.ofp_action_vlan_vid(vlan_vid = 120))
#    packet_out.in_port = 1 #Not necessary, doesn't check it
    packet_out.actions.append(of.ofp_action_output(port = 2))
    q_packetout.put(packet_out)
    proxyctrl_sw.packet_out_send()

  def _rx_flow_mod (self, ofp, connection):
    self.delta_t = time.time()-self.tick
    self.tick = time.time()
    if (self.delta_t > 0.003) and (ofp.command != 3):
      buf_id = copy(ofp.buffer_id)
      ofp.buffer_id = None
      q_flowmod_hw.put(ofp)
#      self.counterdir += 1
#      log.info('%i Flow_Mod-->Hw_Switch (direct)' %self.counterdir)
      proxyctrl_hw.flow_mod_send()
# Packet_out to Sw_Switch instead of Flow_Mod because packet caused Packet_In
#still resides in the Buffer of Sw_Sw (or alternatevily install Flow_Mod ?)
      msg = of.ofp_packet_out()
      msg.buffer_id = buf_id
      if ofp.actions[0].port == 3:
        msg.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
      elif ofp.actions[0].port == 4:
        msg.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
      msg.actions.append(of.ofp_action_output(port = 2))
      msg.in_port = 1
      q_packetout.put(msg)
      proxyctrl_sw.packet_out_send()
#In case of Flow_Mod to SW_Switch
#      actions and match should be rewritten: in_port =1 particularly
#      q_flowmod_sw.put(ofp)
#      proxyctrl_sw.flow_mod_send()
    elif ofp.command == 3:
      q_flowmod_sw.put(ofp)
      proxyctrl_sw.flow_mod_send()
    else:
      ofp_out_act = copy(ofp.actions)
      ofp_out_match = copy(ofp.match)
      ruleextr = ofp.actions.pop()
      if ruleextr.port == 3:
        ofp.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
      elif ruleextr.port == 4:
        ofp.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
      ofp.actions.append(of.ofp_action_output(port = 2))
      ofp.match.in_port = 1 #Conflict with HW_Switch needs to be twice rewriten
      q_flowmod_sw.put(ofp)
      ofp.timestamp = time.time()
      proxyctrl_sw.flow_mod_send()
      ofp.actions = ofp_out_act
      ofp.match = ofp_out_match #or just rewrite match.in_port for speeding up:
#      ofp.match.in_port = 3 #! Find better solution, or don't match port_in at all??
      q_deferred.put(ofp)
      self.counterdef += 1
      log.info('%i Flow_Mod-->Thread' %self.counterdef)



#launching ProxySwitch element to a Controller
def proxyswitch (address='192.168.168.1', port = 6633, max_retry_delay = 16,
    dpid ='7', extra = None, __INSTANCE__ = None):
  dpid = str_to_dpid(dpid)
  port = int(port)
  max_retry_delay = int(max_retry_delay)

  def start_proxyswitch (event):
    def handle_SwitchStarted (event):
      global proxysw
      proxysw = ProxySwitch(dpid = dpid, name="sw"+str(dpid),
      address = address, port = port, max_retry_delay = max_retry_delay)
    log.info("Waiting for connection to switch...")
    waiting_switch.addListenerByName("Started", handle_SwitchStarted)
  core.addListenerByName("UpEvent", start_proxyswitch)

#launching ProxyControllers elements to both switches
def proxycontroller (address='127.0.0.1', port = 6633, __INSTANCE__ = None):
  def start_proxycontroller (event):
    if event.dpid == 3:#Initializing ProxyController for particular HW switch
      global proxyctrl_hw
      proxyctrl_hw = ProxyControllerHw(event.connection)
      #log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
    elif event.dpid == 2:#Initializing ProxyController for particular SW switch
      global proxyctrl_sw
      proxyctrl_sw = ProxyControllerSw(event.connection)
      #log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
      waiting_switch.raiseEvent(Started)
  core.openflow.addListenerByName("ConnectionUp", start_proxycontroller)
