# -*- coding: utf-8 -*-
#!/usr/bin/env python
import socket
import subprocess
import sys
from datetime import datetime
import time

# Clear the screen
subprocess.call('clear', shell=True)

print "-" * 60
print "Opening Sockets for listening"
print "-" * 60

host = socket.gethostbyname('10.0.0.2')

sock = []
i=0

try:
    for port in range(50000,51000):
        sock.append(socket.socket(socket.AF_INET, socket.SOCK_STREAM))
        sock[i].bind((host, port))
        sock[i].listen(1)
        print "Port {}: \t Opened".format(port)
        i+=1

except KeyboardInterrupt:
    print "You pressed Ctrl+C"
    sys.exit()

except socket.gaierror:
    print 'Hostname could not be resolved. Exiting'
    sys.exit()

except socket.error:
    print "Couldn't connect to server"
    sys.exit()

z = raw_input()
print 'Closing Sockets...'

for port in range(50000,51000):
  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  host = socket.gethostbyname('10.0.0.2')
  sock.bind((host, port))
  sock.close()
  print "Port {}: \t Closed".format(port)

