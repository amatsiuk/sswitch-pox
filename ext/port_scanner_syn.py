# -*- coding: utf-8 -*-
#!/usr/bin/env python
import socket
import subprocess
import sys
from datetime import datetime
import time
import threading
import scapy
from scapy.all import *


# Clear the screen
subprocess.call('clear', shell=True)

# Ask for input
#remoteServer    = raw_input("Enter a remote host to scan: ")
remoteServerIP  = socket.gethostbyname('10.0.0.2')

num = raw_input("Enter number of ports to scan:")
num_ports  = int(num)

print "-" * 60
print "Please wait, scanning remote host", remoteServerIP
print "-" * 60

t1 = datetime.now()

class WorkerThread(threading.Thread):

  def __init__(self, i, remoteServerIP):
    super(WorkerThread, self).__init__()
    self.port = i
    self.remoteServerIP = remoteServerIP

  def run(self):

    try:
      e = Ether()
      e.type = 0x800
      e.src = '00:00:00:00:00:01'
      e.dst = '00:00:00:00:00:02'
      i = IP()
      i.src = "10.0.0.1"
      i.dst = "10.0.0.2"
      t = TCP()
      t.sport = random.randint(50000,65535)
      t.dport = int(self.port)
      t.flags = 'S'
      scapy.all.srp((e/i/t), iface = "h1-eth0")


    except KeyboardInterrupt:
      print "You pressed Ctrl+C"
      self.join()

  def join(self, timeout=None):
    super(WorkerThread, self).join(timeout)

try:

  pool= [WorkerThread(i, remoteServerIP) for i in range(50000,50000+num_ports)]

  for thread in pool:
    thread.start()

except KeyboardInterrupt:
    print "You pressed Ctrl+C"
    sys.exit()


print "waitng threads..."
for thread in pool:
  thread.join()


t2 = datetime.now()
total =  t2 - t1
print 'Scanning Completed in: ', total