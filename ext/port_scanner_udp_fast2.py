# -*- coding: utf-8 -*-
#!/usr/bin/env python
import socket
import subprocess
import sys
from datetime import datetime
import time
import threading, Queue

timestamp_q =Queue.Queue()

# Clear the screen
subprocess.call('clear', shell=True)

remoteServerIP  = socket.gethostbyname('10.0.0.2')

num = raw_input("Enter number of ports to scan:")
numpack = raw_input("Enter number of packets to send:")
num_ports  = int(num)
num_pack = int(numpack)
#num_bytes = 1384
num_bytes = 40

print "-" * 60
print "Please wait, scanning remote host", remoteServerIP
print "-" * 60


#t1 = datetime.now()
t1 = time.time()
lock = threading.Lock()


class WorkerThread(threading.Thread):

  def __init__(self, i, num_pack, num_bytes, remoteServerIP, num_ports):
    super(WorkerThread, self).__init__()
    self.port = i
    self.num_pack = num_pack
    self.remoteServerIP = remoteServerIP
    self.str_to_send = '1'*num_bytes
    self.num_ports = num_ports

  def run(self):

    try:
      sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      for i in range(0, self.num_pack):
        sock.sendto(self.str_to_send, (self.remoteServerIP, self.port))
        if (self.port == 30000 and i == 0) or (self.port == 30000+self.num_ports-1 and i == self.num_pack-1):
          timestamp_q.put(time.time())
#        time.sleep(0.01)
      sock.close()

    except socket.gaierror:
      print 'Hostname could not be resolved. Exiting'
      sys.exit()

    except socket.error:
      print "Couldn't connect to server"
      sys.exit()

    except KeyboardInterrupt:
      print "You pressed Ctrl+C"
      sys.exit()

  def join(self, timeout=None):
    super(WorkerThread, self).join(timeout)

try:

  pool= [WorkerThread(i, num_pack, num_bytes, remoteServerIP, num_ports) for i in range(30000,30000+num_ports)]

  for thread in pool:
#    time.sleep(0.0008)
    thread.start()

except KeyboardInterrupt:
    print "You pressed Ctrl+C"
    sys.exit()

except socket.gaierror:
    print 'Hostname could not be resolved. Exiting'
    sys.exit()

except socket.error:
    print "Couldn't connect to server"
    sys.exit()

for thread in pool:
  thread.join()

t1 = timestamp_q.get()
t2 = timestamp_q.get()
deltat = t2 - t1
print 'Scanning Completed in:',deltat
