#!/usr/bin/python
from scapy.all import *
import sys
from datetime import datetime
import time
from copy import copy,deepcopy

#num_packets = int(raw_input("Enter number of packets to send:"))
num_packets = int(sys.argv[1])
list_number = int(sys.argv[2])

l2_part = Ether(src='00:9c:02:ad:4e:1e', dst='68:05:ca:10:a4:b4',type=0x0800)
l3_part = IP(src='10.0.0.1',dst='10.0.0.2')
l4_part = UDP(sport=30000,dport=10000)
l7_part = "test1111test1111test00"
packet = l2_part/l3_part/l4_part/l7_part
packetlist = []

t1 = time.time()


try:
  for i in range(0, num_packets):
    packetlist.append(deepcopy((packet)))
  print "packetlist created"
  for i in xrange(len(packetlist)):
    packetlist[i].dport = (10000 + i) % 60000
#    print packetlist[i].dport
#  print packetlist
  wrpcap("/home/mininet/scripts/floodfile%s.pcap" %str(list_number) ,packetlist)


#  packetlistsend = rdpcap("/home/mininet/floodfile.pcap")
#  sendpfast(packetlistsend, pps=1000000, iface='eth1',)
#  sendp(packet, iface='br0',verbose=False)

except KeyboardInterrupt:
  print "You pressed Ctrl+C"
  sys.exit()
t2 = time.time()
deltat = t2 - t1
print 'Creation Completed in:',deltat