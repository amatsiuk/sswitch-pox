from pox.core import core
import pox.openflow.libopenflow_01 as of

#Test L4 controller with reduced functionality and faster reaction;
#doesn't analyze incoming port,Sends all packets to port 4 of HwSw,
#changes udp.dstport in consequent FTEs
#works in conjunction with Proxy

log = core.getLogger()

#Constants:
proxy_dpid = 7 #DPID of Proxy

class TestController (object):

  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    self.mac_to_port = {}
    self.dstport_to_port = {}
    self.out_port = 4
    self.msg = of.ofp_flow_mod()
    self.msg.priority = 25000
    self.msg.match.in_port = 3
    self.msg.match.dl_type = 0x0800
    self.msg.match.nw_proto = 17
    self.msg.idle_timeout = 0
    self.msg.hard_timeout = 360
    self.msg.actions.append(of.ofp_action_output(port = self.out_port))

  def act_like_switch (self, packet, packet_in):
    dstport = packet.next.next.next.dstport
    if dstport not in self.dstport_to_port:
      self.dstport_to_port[dstport] = self.out_port
      self.msg.match.tp_dst = dstport
      self.connection.send(self.msg)
    else:
        pass  #Ignore consequent packets of the same flow

  def _handle_PacketIn (self, event):
    packet = event.parsed
    packet_in = event.ofp
    self.act_like_switch(packet, packet_in)


def launch ():
  def start_switch (event):
    if event.dpid == proxy_dpid:
      log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
      global testcontroller
      testcontroller = TestController(event.connection)
    else:
     log.info("Ignore conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
  core.openflow.addListenerByName("ConnectionUp", start_switch)