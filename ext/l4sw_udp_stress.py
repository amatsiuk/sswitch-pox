from pox.core import core
import pox.openflow.libopenflow_01 as of
import time

log = core.getLogger()

class TestController (object):

  def __init__ (self, connection):

    self.connection = connection
    connection.addListeners(self)
    self.mac_to_port = {}
    self.dstport_to_port = {}
    self.countermod = 0
    self.hard_timeout = 30


  def act_like_switch (self, packet, packet_in):

    if str(packet.src) not in self.mac_to_port:
      log.info('NEW MAC: %s' %str(packet.src))
      self.mac_to_port[str(packet.src)] = packet_in.in_port
      if packet_in.in_port == 3:
        self.mac_to_port[str(packet.dst)] = 4
      elif packet_in.in_port == 4:
        self.mac_to_port[str(packet.dst)] = 3
      else:
        log.info('UNKNOWN In_Port: %s' %str(packet_in.in_port))

    self.countermod += 1
    packet3next = packet.next.next.next
    log.info(" %i Flow_Mod: -> %s:%i port %i"
    %(self.countermod, packet.dst, packet3next.dstport, 4))
    self.dstport_to_port[packet3next.dstport] = self.mac_to_port[str(packet.dst)]
    msg = of.ofp_flow_mod()
    msg.match.in_port = packet_in.in_port
    msg.match.dl_type = 0x0800
    msg.match.nw_proto = 17
    msg.match.tp_dst = packet3next.dstport
    msg.match.tp_src = packet3next.srcport
    msg.idle_timeout = 0
    msg.hard_timeout = self.hard_timeout
    msg.actions.append(of.ofp_action_output(port = self.mac_to_port[str(packet.dst)]))
    msg.buffer_id = packet_in.buffer_id
    time.sleep(2)
    self.connection.send(msg)



  def _handle_PacketIn (self, event):
    packet = event.parsed 
    packet_in = event.ofp
    self.act_like_switch(packet, packet_in)


def launch ():

  def start_switch (event):
    log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
    TestController(event.connection)
  core.openflow.addListenerByName("ConnectionUp", start_switch)
