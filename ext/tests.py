#!/usr/bin/env python

from time import sleep
import subprocess
from subprocess import Popen
import sys, os
import logging

# Configurations
dpdk_ovs_root = '/root/skuenzer/intel-ovdk/openvswitch'
dpdk_qemu_root = '/root/skuenzer/intel-ovdk/qemu'
src_port = '17'
dst_port = '16'
hostname = "computer43"
username = "root"

# Static conf
dpdk_ovs = '%s/datapath/dpdk/build/ovs_dpdk' % dpdk_ovs_root
vswitchd = '%s/vswitchd/ovs-vswitchd' % dpdk_ovs_root
ofctl = '%s/utilities/ovs-ofctl' % dpdk_ovs_root
out_dir = '/root/skuenzer/results'
ping_sh = '/root/workspace/intel-ovdk-eval/slave-bin/ping_test.sh'
ping2_sh = '/root/workspace/intel-ovdk-eval/slave-bin/ping2_test.sh'
ping_tr_sh = "/root/workspace/intel-ovdk-eval/slave-bin/ping-tr_test.sh"
throughput_sh = '/root/workspace/intel-ovdk-eval/slave-bin/throughput_test.sh'
ivshm_vm_start_sh = '%s/run_qemu_ivshm-multi.sh' % dpdk_qemu_root

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger('tester')


def exc(cmd):
  return Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) 

def excr(cmd, hostname="computer43", username="root"):
    os.system('ssh -t %s@%s "%s"' % (username, hostname, cmd) )



def run_switch(sw_core, port1_core, port2_core):
  logger.info("Starting switch")
  cmd = '%s -c 0xf -n 4 --proc-type=primary --huge-dir /mnt/huge -- -p 0x03 -k 2 -n 3 --stats=0 --vswitchd=0 --client_switching_core=%s --config="(0,0,%s),(1,0,%s) &"'% (dpdk_ovs, sw_core, port1_core, port2_core)
  proc = exc(cmd)
  sleep(25)
  exc("/bin/cp -a /var/run/.rte_* /tmp/share/")

def stop_switch():
  logging.info("Stopping switch")
  cmd = "killall ovs_dpdk"
  proc = exc(cmd)
  sleep(2)

def run_vswitchd():
  logging.info("Starting vswitchd")
  cmd = "%s -c 1 --proc-type=secondary &" % vswitchd
  proc = exc(cmd)
  sleep(4)

def stop_vswitchd():
  logging.info("Stopping vswitchd")
  cmd = "killall -9 ovs-vswitchd"
  exc(cmd)
  sleep(2)

def install_rules(type):
  logging.info("Installing rules for %s" % type)
  
  exc("%s del-flows br0" % ofctl)
  if type == "switch":
    exc('%s add-flow br0 in_port=%s,idle_timeout=0,action=output:%s' % (ofctl, src_port, dst_port))
  elif type == "vm":
    exc('%s add-flow br0 in_port=%s,dl_type=0x0800,nw_dst=10.0.0.3,idle_timeout=0,action=output:1' % (ofctl, src_port))
    exc('%s add-flow br0 in_port=1,idle_timeout=0,action=output:%s' % (ofctl, dst_port))
    exc('%s add-flow br0 in_port=%s,dl_type=0x0800,nw_dst=10.0.0.4,idle_timeout=0,action=output:2' % (ofctl, src_port))
    exc('%s add-flow br0 in_port=2,idle_timeout=0,action=output:%s' % (ofctl, dst_port))
  else:
    logging.info("I don't know how to install rules for argument %s" % type)

def initialize_system(sw_core, port1_core, port2_core, type):
  run_switch(sw_core, port1_core, port2_core)
  run_vswitchd()
  install_rules(type)
  
def dispose_system():
  stop_vswitchd()
  stop_switch()

def run_ivshm_vm(core_mask, id, run_dpdk, second=False):
  logging.info("Running VM%s %s" % (id, core_mask))
  if run_dpdk:
    exc("touch /tmp/share/run")
  if second:
    exc("touch /tmp/share/second")
  exc("%s %s %s" % (ivshm_vm_start_sh, core_mask, id) ) 
  sleep(20)
  exc("rm /tmp/share/run")
  exc("rm /tmp/share/second")

def stop_ivshm_vm():
  logging.info("Stopping VMs")
  exc("killall qemu-system-x86_64")
  sleep(2)

def run_ping_tests(dir, ping_script):
  odir = "%s/%s" % (out_dir, dir)
  excr("mkdir -p %s" % (odir))
  excr("cp %s %s/test.sh" % (ping_script, odir) ) 
  excr("ping 10.0.0.3 -I ixgbe.0 -c 10") # avoiding first packet spike
  logging.info("Running ping test")
  excr("%s/test.sh %s" % (odir, odir) )

def run_throughput_tests(dir, dst_addr):
  logging.info("Running throughput tests")
  odir = "%s/%s" % (out_dir, dir)    
  excr("mkdir -p %s" % (odir))
  excr("%s %s %s/throughput.txt" % (throughput_sh, dst_addr, odir))
  
def test_switch(sw_core, port1_core, port2_core, odir):
  logging.info("Testing switch: %s" % odir)
  initialize_system(sw_core, port1_core, port2_core, "switch")
  logging.info("Running tests")
  run_ping_tests("switch/%s" % odir, ping_sh)
  run_throughput_tests("switch/%s" % odir, "10.0.0.3:10")
  dispose_system() 

def test_ivshm(sw_core, port1_core, port2_core, vm_core, odir):
  logging.info("Testing switch: %s" % odir)
  initialize_system(sw_core, port1_core, port2_core, "vm")
  run_ivshm_vm(vm_core, "1", True)
  logging.info("Running tests")
  run_ping_tests("ivshm/%s" % odir, ping_sh)
  run_throughput_tests("ivshm/%s" % odir, "10.0.0.3:10")
  stop_ivshm_vm()
  dispose_system() 

def test_ivshm2VM(sw_core, port1_core, port2_core, vm1_core, vm2_core, test, odir):
  logging.info("Testing switch: %s %s" % (odir, test))
  initialize_system(sw_core, port1_core, port2_core, "vm")
  run_ivshm_vm(vm1_core, "1", True)

  dst_addr = '10.0.0.3:10'

  if test == "2working":
    run_ivshm_vm(vm2_core, "2", True, True)
    dst_addr = '10.0.0.3:10-10.0.0.4:10'
    excr("ping 10.0.0.4 -I ixgbe.0 -c 10") # avoiding first packet spike
    run_ping_tests("ivshm2VM/%s-%s" % (odir, test), ping2_sh)
    run_throughput_tests("ivshm2VM/%s-%s" % (odir, test), dst_addr)
  elif test == "1working":
    run_ivshm_vm(vm2_core, "2", False, True)
    run_ping_tests("ivshm2VM/%s-%s" % (odir, test), ping_sh)
    run_throughput_tests("ivshm2VM/%s-%s" % (odir, test), dst_addr)
  elif test == "1working-1dpdkidle":
    run_ivshm_vm(vm2_core, "2", True, True)
    run_ping_tests("ivshm2VM/%s-%s" % (odir, test), ping_sh)
    run_throughput_tests("ivshm2VM/%s-%s" % (odir, test), dst_addr)
  elif test == "network":
    run_ivshm_vm(vm2_core, "2", True, True)
    excr("ping 10.0.0.4 -I ixgbe.0 -c 10") # avoiding first packet spike
    run_ping_tests("ivshm2VM/%s-%s" % (odir, test), ping_tr_sh)
  else:
    logging.error("Unrecognized test for test_ivshm2VM")
  
  stop_ivshm_vm()
  dispose_system() 

test_switch(1,2,3,"3cores")
test_switch(1,2,2,"2cores")
test_switch(1,1,1,"1core")

"""
test_ivshm(1,2,3, "0x10", "3cores-1VM")
test_ivshm(1,2,2, "0x10", "2cores-1VM")
test_ivshm(1,1,1, "0x10", "1core-1VM")
"""
test_ivshm(1,2,3, "0x30", "3cores-1VM")
test_ivshm(1,2,2, "0x30", "2cores-1VM")
test_ivshm(1,1,1, "0x30", "1core-1VM")
"""
test_ivshm2VM(1,2,3, "0x10", "0x20", "2working", "3cores-2VM")
test_ivshm2VM(1,2,3, "0x10", "0x20", "1working","3cores-2VM")
test_ivshm2VM(1,2,3, "0x10", "0x20", "1working-1dpdkidle","3cores-2VM")
test_ivshm2VM(1,2,3, "0x10", "0x20", "network", "3cores-2VM")

test_ivshm2VM(1,2,2, "0x10", "0x20", "2working", "2cores-2VM")
test_ivshm2VM(1,2,2, "0x10", "0x20", "1working","2cores-2VM")
test_ivshm2VM(1,2,2, "0x10", "0x20", "1working-1dpdkidle","2cores-2VM")
test_ivshm2VM(1,2,2, "0x10", "0x20", "network", "2cores-2VM")

test_ivshm2VM(1,1,1, "0x10", "0x20", "2working", "1core-2VM")
test_ivshm2VM(1,1,1, "0x10", "0x20", "1working","1core-2VM")
test_ivshm2VM(1,1,1, "0x10", "0x20", "1working-1dpdkidle","1core-2VM")
test_ivshm2VM(1,1,1, "0x10", "0x20", "network", "1core-2VM")


test_ivshm2VM(1,2,3, "0x10", "0x10", "2working", "3cores-2VM-1core")

test_ivshm2VM(1,2,3, "0x10", "0x10", "1working","3cores-2VM-1core")
test_ivshm2VM(1,2,3, "0x10", "0x10", "1working-1dpdkidle","3cores-2VM-1core")
test_ivshm2VM(1,2,3, "0x10", "0x10", "network", "3cores-2VM-1core")

test_ivshm2VM(1,2,2, "0x10", "0x10", "2working", "2cores-2VM-1core")
test_ivshm2VM(1,2,2, "0x10", "0x10", "1working","2cores-2VM-1core")
test_ivshm2VM(1,2,2, "0x10", "0x10", "1working-1dpdkidle","2cores-2VM-1core")
test_ivshm2VM(1,2,2, "0x10", "0x10", "network", "2cores-2VM-1core")

test_ivshm2VM(1,1,1, "0x10", "0x10", "2working", "1core-2VM-1core")
test_ivshm2VM(1,1,1, "0x10", "0x10", "1working","1core-2VM-1core")
test_ivshm2VM(1,1,1, "0x10", "0x10", "1working-1dpdkidle","1core-2VM-1core")
test_ivshm2VM(1,1,1, "0x10", "0x10", "network", "1core-2VM-1core")
"""
