from pox.core import core
import pox.openflow.libopenflow_01 as of
import time


#Offloads all traffic from HwSw to SwSw: with L4 rules in OVS
log = core.getLogger()


#Constants:
hwsw_dpid = 3 #DPID of HwSw
swsw_dpid = 2 #DPID of SwSw
num_flows = 10000 #number of static flows starting udp.dstport = 10,000


class ProxyControllerHw (object):

  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    self.act_like_switch()


  def act_like_switch (self):
    msg1 = of.ofp_flow_mod()
    msg1.match.in_port = 3
    msg1.idle_timeout = 0
    msg1.hard_timeout = 0
    msg1.actions.append(of.ofp_action_vlan_vid(vlan_vid = 10))
    msg1.actions.append(of.ofp_action_output(port = 47))
    self.connection.send(msg1)
    log.info("Flow_Mod: 3 -> 47")
    msg2 = of.ofp_flow_mod()
    msg2.match.in_port = 4
    msg2.idle_timeout = 0
    msg2.hard_timeout = 0
    msg2.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
    msg2.actions.append(of.ofp_action_output(port = 47))
    self.connection.send(msg2)
    log.info("Flow_Mod: 4 -> 47")
    msg3 = of.ofp_flow_mod()
    msg3.match.in_port = 48
    msg3.match.dl_vlan = 10
    msg3.idle_timeout = 0
    msg3.hard_timeout = 0
    msg3.actions.append(of.ofp_action_strip_vlan())
    msg3.actions.append(of.ofp_action_output(port = 3))
    self.connection.send(msg3)
    log.info("Flow_Mod: 48 -> 3")
    msg4 = of.ofp_flow_mod()
    msg4.match.in_port = 48
    msg4.match.dl_vlan = 20
    msg4.idle_timeout = 0
    msg4.hard_timeout = 0
    msg4.actions.append(of.ofp_action_strip_vlan())
    msg4.actions.append(of.ofp_action_output(port =4))
    self.connection.send(msg4)
    log.info("Flow_Mod: 48 -> 4")


class ProxyControllerSw (object):

  def __init__ (self, connection):
    self.connection = connection
    connection.addListeners(self)
    self.act_like_switch()

  def act_like_switch (self):
    msg = of.ofp_flow_mod()
    msg.match.in_port = 1
    msg.match.dl_vlan = 10
    msg.match.dl_type = 0x0800
    msg.match.nw_proto = 17
    msg.match.tp_dst = 10000
    msg.idle_timeout = 0
    msg.hard_timeout = 0
#    msg.priority = 25000
    msg.actions.append(of.ofp_action_vlan_vid(vlan_vid = 20))
    msg.actions.append(of.ofp_action_output(port = 2))
    for i in range (10000, 10000+num_flows):
      msg.match.tp_dst = i
      self.connection.send(msg)
#      log.info("Flow_Mod: 3 -> 4:%i" %i)
    self.connection.send(of.ofp_barrier_request(xid = msg.xid))
    self.t2 = time.time()

  def _handle_BarrierIn (self, event):
    deltat2 = time.time() - self.t2
    log.info("Barrier Reply xid= %i returned in: %.4f" %(event.xid, deltat2))


def launch ():

  def start_switch (event):
    if event.dpid == hwsw_dpid:#Initializing ProxyController for HwSw
      global proxyctrl_hw
      proxyctrl_hw = ProxyControllerHw(event.connection)
      log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
    elif event.dpid == swsw_dpid:#Initializing ProxyController for SwSw
      global proxyctrl_sw
      proxyctrl_sw = ProxyControllerSw(event.connection)
      log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
    else:
     log.info("Ignore conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
  core.openflow.addListenerByName("ConnectionUp", start_switch)

