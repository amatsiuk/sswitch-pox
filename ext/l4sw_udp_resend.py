from pox.core import core
import pox.openflow.libopenflow_01 as of
import time

log = core.getLogger()

class TestController (object):

  def __init__ (self, connection):

    self.connection = connection
    connection.addListeners(self)
    self.mac_to_port = {}
    self.dstport_to_port = {}
    self.counterpout = 0
    self.countermod = 0
    self.hard_timeout = 30
    self.tick = time.time()


  def resend_packet (self, packet, out_port):

    msg = of.ofp_packet_out()
    msg.buffer_id = None
    if out_port == 4:
      msg.in_port = 3
    elif out_port == 3:
      msg.in_port = 4
    msg.data = packet
    action = of.ofp_action_output(port = out_port)
    msg.actions.append(action)
    self.connection.send(msg)
    self.counterpout +=1
    log.info('%i Packet_Out: -> port %i' %(self.counterpout, out_port))

  def act_like_switch (self, packet, packet_in):

    if time.time() - self.tick > self.hard_timeout:
      self.dstport_to_port.clear()

    if str(packet.src) not in self.mac_to_port:
      log.info('NEW MAC: %s' %str(packet.src))
      self.mac_to_port[str(packet.src)] = packet_in.in_port
      if packet_in.in_port == 3:
        self.mac_to_port[str(packet.dst)] = 4
      elif packet_in.in_port == 4:
        self.mac_to_port[str(packet.dst)] = 3
      else:
        log.info('UNKNOWN In_Port: %s' %str(packet_in.in_port))

#    port = self.mac_to_port[str(packet.dst)]
    packet3next = packet.next.next.next
    self.tick = time.time()
#    packet3next = packet.next.next

    if packet3next.dstport not in self.dstport_to_port:
      self.countermod += 1
      log.info(" %i Flow_Mod: -> %s:%i port %i"
      %(self.countermod, packet.dst, packet3next.dstport, 4))
      self.dstport_to_port[packet3next.dstport] = self.mac_to_port[str(packet.dst)]
      msg = of.ofp_flow_mod()
      msg.match.in_port = packet_in.in_port
#      msg.match.dl_dst = packet.dst
#      msg.match.dl_src = packet.src
      msg.match.dl_type = 0x0800
      msg.match.nw_proto = 17
      msg.match.tp_dst = packet3next.dstport
#      msg.match.tp_src = packet3next.srcport
      msg.idle_timeout = 0
      msg.hard_timeout = self.hard_timeout
      msg.actions.append(of.ofp_action_output(port = self.mac_to_port[str(packet.dst)]))
      msg.buffer_id = None
#      msg.buffer_id = packet_in.buffer_id
      self.connection.send(msg)
    else:
      if self.dstport_to_port[packet3next.dstport] == self.mac_to_port[str(packet.dst)]:
        self.counterpout += 1
        log.info("%i Packet_Out for Port: %i", self.counterpout, packet3next.dstport)
        msg = of.ofp_packet_out()
        msg.data = packet_in
        action = of.ofp_action_output(port = self.mac_to_port[str(packet.dst)])
        msg.actions.append(action)
        self.connection.send(msg)
        #pass
      else:
        log.info("Duplicated Port: %i", packet3next.dstport)


  def _handle_PacketIn (self, event):
#    print '%.6f' %time.time(), '-->l2sw'
    packet = event.parsed # This is the parsed packet data.
# Without VLAN Tag 2nd next derivative should be used!
#    packet2next = packet.next.next
#    print '2nd next type, dict:',type(packet2next), packet2next.__dict__
#    if hasattr(packet2next, 'dstport'):
#      print "-" * 20
#      print 'src_port:', packet2next.srcport
#      print 'dst_port:', packet2next.dstport
    packet_in = event.ofp
    if packet_in.in_port == 3:
      out_port = 4
    elif packet_in.in_port == 4:
      out_port = 3
    else:
      print "Unknown In_Port:", packet_in.in_port
    self.resend_packet(packet, out_port)


def launch ():

  def start_switch (event):
    log.info("Controlling conID=%s, dpid=%i" %(event.connection.ID, event.dpid))
    TestController(event.connection)
  core.openflow.addListenerByName("ConnectionUp", start_switch)
