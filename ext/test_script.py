#!/usr/bin/env python

import sys
from time import sleep
import logging
from pxssh import pxssh, ExceptionPxssh
import pexpect

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger('tester')

# Servers configuration parameters Server1: Generator, Server2: SWBRAS
server1='10.15.254.149'
username1='root'
password1='generator'
server2='10.15.254.148'
username2='root'
password2='swbras'
basicpox='python pox.py --unthreaded_sh log.level --WARN'
loopback1='127.0.0.1'
loopback2='192.168.168.1'
server1_uplink='eth2'
server1_downlink='eth3'
server2_send ='eth1'
server2_recv ='eth3'
server1_workdir = "/home/mininet/pox"
server2_workdir = "/home/mininet/scripts"
server2_mountdir = "/var/ramfs" #change together with source_dir constant
#in port_scanner.py and csv_processor.py if needed

frame_size = int(sys.argv[1])
flow_number = int(sys.argv[2]) # Only 1000 or 10,000 are possible
#otherwise remove restriction in port_scanner.py
test_number =4


def init_server(hostname, username, password, cmd):
  try:
    s = pxssh()
    if hostname == server1:
      s.logfile = open("script.log","w")
    elif hostname == server2:
      s.logfile = open("script.log","a+")
    else:
      logging.warn("unknown server!")
    s.login(hostname, username, password)
    for line in cmd:
      s.sendline(line)
      sleep(2)
    s.logout()
  except ExceptionPxssh, e:
      print "SSH failed to login."
      print str(e)

def clean_server(hostname, username, password, cmd):
  try:
    s = pxssh()
    s.logfile = open("script.log","a+")
    s.login(hostname, username, password)
    for line in cmd:
      s.sendline(line)
      sleep(2)
    s.logout()
  except ExceptionPxssh, e:
      print "SSH failed to login."
      print str(e)

def exc_controller(hostname, username, password, cmd):
  try:
    s = pxssh()
    s.logfile = open("script.log","a+")
    s.login(hostname, username, password)
    for line in cmd:
      s.sendline(line)
      sleep(2)
    s.logout()
  except ExceptionPxssh, e:
      print "SSH failed to login."
      print str(e)

def exc_generator(hostname, username, password, cmd):
  try:
    s = pxssh()
    s.logfile = open("script.log","a+")
    s.timeout = 300
    s.login(hostname, username, password)
    for line in cmd:
      s.sendline(line)
      sleep(2)
    logging.info('Waiting flow_generator.py')
    sleep(1)
    s.expect("scanning completed")
    logging.info('scanning completed')
    sleep(2)
    s.sendline("kill -SIGHUP %1")
    sleep(2)
    s.logout()
  except ExceptionPxssh, e:
      print "SSH failed to login."
      print str(e)

def exc_tshark(hostname, username, password, cmd):
  try:
    s = pxssh()
    s.logfile = open("script.log","a+")
    s.login(hostname, username, password)
    for line in cmd:
      s.sendline(line)
      sleep(2)
    logging.info('Waiting tshark parsing')
    sleep(420)
    s.sendline("rm -rf *.pcap")
    sleep(2)
    logging.info('tshark parsing completed')
    s.logout()
  except ExceptionPxssh, e:
      print "SSH failed to login."
      print str(e)

def exc_graph_plotter(hostname, username, password, cmd):
  try:
    s = pxssh()
    s.logfile = open("script.log","a+")
    s.timeout = 300
    s.login(hostname, username, password)
    for line in cmd:
      s.sendline(line)
      sleep(2)
    s.expect("plotting completed")
    logging.info('graphs plotting completed')
    sleep(2)
    s.logout()
  except ExceptionPxssh, e:
      print "SSH failed to login."
      print str(e)

def start_init_server1():
  logging.info("Initializing Server1 (Generator)")
  cmd=[]
  cmd.append("ifconfig lo:1 up")
  cmd.append("ovs-vsctl del-br br0")
  cmd.append("ovs-vsctl add-br br0")
  cmd.append("ovs-vsctl set-controller br0 tcp:127.0.0.1:6633 ptcp:6634")
  cmd.append("ovs-vsctl set bridge br0 other-config:datapath-id=0000000000000002")
  cmd.append("ovs-vsctl set bridge br0 other-config:disable-in-band=true")
  cmd.append("ovs-vsctl add-port br0 %s" %(server1_uplink))
  cmd.append("ovs-vsctl add-port br0 %s" %(server1_downlink))
  init_server(server1, username1, password1, cmd)

def start_init_server2():
  logging.info("Initializing Server2 (SWBRAS)")
  cmd=[]
  cmd.append("ifconfig %s down" %(server2_send))
  cmd.append("ifconfig %s up" %(server2_send))
  cmd.append("ifconfig %s down" %(server2_recv))
  cmd.append("ifconfig %s up" %(server2_recv))
  cmd.append("arp -s -i %s 10.0.0.2 00:9c:02:ad:4e:22" %(server2_send))
  cmd.append("ip route add 10.0.0.2/32 via 10.0.0.1 dev %s" %(server2_send))
  cmd.append("ifconfig %s promisc" %(server2_recv))
  cmd.append("mkdir %s" %(server2_mountdir))
  cmd.append("umount %s" %(server2_mountdir))
  cmd.append("mount -t tmpfs -o size=5000m tmpfs %s" %(server2_mountdir))
  cmd.append("cd %s" %(server2_mountdir))
  cmd.append("rm -f *")
  cmd.append("KPID=`ps -eo pid,command | grep tcpdump | grep -v grep | awk '{print $1}'`")
  cmd.append("kill -SIGHUP $KPID")
  cmd.append("KPID=`ps -eo pid,command | grep tshark | grep -v grep | awk '{print $1}'`")
  cmd.append("kill -SIGHUP $KPID")
  init_server(server2, username2, password2, cmd)

def start_clean_server1():
  logging.info("Cleaning Server1 (Generator)")
  cmd=[]
  cmd.append("KPID=`ps -eo pid,command | grep pox.py | grep -v grep | awk '{print $1}'`")
  cmd.append("kill -SIGHUP $KPID")
  clean_server(server1, username1, password1, cmd)

def start_clean_server2():
  logging.info("Cleaning Server2 (SWBRAS)")
  cmd=[]
  cmd.append("cd %s" %(server2_mountdir))
  cmd.append("rm -f *")
  cmd.append("cd ..")
  cmd.append("umount %s" %(server2_mountdir))
  cmd.append("rmdir %s" %(server2_mountdir))
  clean_server(server2, username2, password2, cmd)

def start_test_controller(controller_type, address):
  logging.info("Starting controller: %s" %(controller_type))
  cmd=[]
  cmd.append("cd %s" %server1_workdir)
  cmd.append("KPID=`ps -eo pid,command | grep pox.py | grep -v grep | awk '{print $1}'`")
  cmd.append("kill -SIGHUP $KPID")
  cmd.append("%s %s %s &" %(basicpox, controller_type, address))
  exc_controller(server1, username1, password1, cmd)
  logging.info("Waiting flows installed")
  if controller_type == "transit_l4_hw":
    sleep(17)
  else:
    sleep(5)

def start_proxy_controller(controller_type, address1, proxy_type, address2):
  logging.info("Starting proxy and controller: %s" %(controller_type))
  cmd=[]
  cmd.append("cd %s" %server1_workdir)
  cmd.append("KPID=`ps -eo pid,command | grep pox.py | grep -v grep | awk '{print $1}'`")
  cmd.append("kill -SIGHUP $KPID")
  cmd.append("%s %s %s &" %(basicpox, controller_type, address1))
  cmd.append("%s %s %s &" %(basicpox, proxy_type, address2))
  exc_controller(server1, username1, password1, cmd)
  logging.info("Waiting proxy-controller started")
  sleep(10)

def start_flow_generator(pack_size, flow_num, test_num):
  logging.info("Starting generator number:%i" %(test_num))
  cmd=[]
  cmd.append("tcpdump -B 2050000 -Knn -s 42 -i eth3 -w %s/test_%i.pcap &"
  %(server2_mountdir, test_num))
  cmd.append("cd %s" %server2_workdir)
  cmd.append("python port_scanner.py %i %i %i &" %(pack_size, flow_num, test_num))
  exc_generator(server2, username2, password2, cmd)

def start_tshark(test_num):
  logging.info("Starting tshark for %i tests" %(test_num))
  cmd=[]
  cmd.append("KPID=`ps -eo pid,command | grep tshark | grep -v grep | awk '{print $1}'`")
  cmd.append("kill -SIGHUP $KPID")
  cmd.append("cd %s" %(server2_mountdir))
  for i in range(1, test_num+1):
    cmd.append("tshark -r test_%i.pcap -T fields -e frame.number -e frame.time_relative  -e frame.time_epoch -e udp.dstport > test_%i.csv &"
    %(i,i))
  exc_tshark(server2, username2, password2, cmd)

def start_graph_plotter(pack_size, flow_num, test_num, test_type):
  logging.info("Starting graph plotter for %i %s tests" %(test_num, test_type))
  cmd=[]
  cmd.append("cd %s" %server2_workdir)
  cmd.append("python csv_processor.py %i %i %i %s &" %(pack_size, flow_num, test_num, test_type))
  exc_graph_plotter(server2, username2, password2, cmd)

def start_scp_copier(hostname, username, password):
  copier = pexpect.spawn('scp %s@%s:%s/*.png .' %(username, hostname, server2_mountdir))
  copier.logfile = open("script.log","a+")
  copier.expect('password:')
  copier.sendline (password)
  copier.expect(pexpect.EOF)
  logging.info('copy of graphs completed')

#Uncomment first block to run proactive tests, or second, to run reactive tests
"""
logging.info("starting proactive tests: %i bytes, %i flows" %(frame_size, flow_number))
start_init_server1()
start_init_server2()
start_test_controller("transit_l1_hw","openflow.of_01 --address=%s" %(server1))
start_flow_generator(frame_size, flow_number,1)
start_test_controller("transit_l1_hw+sw", "openflow.of_01 --address=%s openflow.of_01 --address=%s" %(server1, loopback1))
start_flow_generator(frame_size, flow_number,2)
start_test_controller("transit_l4_hw", "openflow.of_01 --address=%s" %(server1))
start_flow_generator(frame_size, flow_number,3)
start_test_controller("transit_l4_hw+sw", "openflow.of_01 --address=%s openflow.of_01 --address=%s" %(server1, loopback1))
start_flow_generator(frame_size, flow_number,4)
start_tshark(test_number)
start_graph_plotter(frame_size, flow_number, test_number, "proactive")
start_scp_copier(server2, username2, password2)
start_clean_server1()
start_clean_server2()
logging.info("proactive tests are done!")
"""
logging.info("starting reactive tests: %i bytes, %i flows" %(frame_size, flow_number))
start_init_server1()
start_init_server2()
start_test_controller("transit_l4_hw","openflow.of_01 --address=%s" %(server1))
start_flow_generator(frame_size, flow_number,1)
start_test_controller("l4sw_hw", "openflow.of_01 --address=%s" %(server1))
start_flow_generator(frame_size, flow_number,2)
start_test_controller("l4sw_sw", "openflow.of_01 --address=%s openflow.of_01 --address=%s" %(server1, loopback1))
start_flow_generator(frame_size, flow_number,3)
start_proxy_controller("l4sw", "openflow.of_01 --address=%s" %(loopback2),
  "proxy:proxyswitch proxy:proxycontroller", "openflow.of_01 --address=%s openflow.of_01 --address=%s" %(server1, loopback1))
start_flow_generator(frame_size, flow_number,4)
start_tshark(test_number)
start_graph_plotter(frame_size, flow_number, test_number, "reactive")
start_scp_copier(server2, username2, password2)
start_clean_server1()
start_clean_server2()
logging.info("reactive tests are done!")
