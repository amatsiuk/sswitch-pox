#!/usr/bin/env python
import sys
import os
import time
import subprocess

def call_ssh(hostname, username, cmd):
    os.system('ssh -t %s@%s "%s"' % (username, hostname, cmd) )
    #return subprocess.call(['ssh', hostname, cmd])

def pkt_pair_test(result_file, result_file2, hostname, username, delay):
  tcpdump_cmd = "nohup ./call_tcpdump.sh %s %s"
  call_ssh(hostname, username, tcpdump_cmd % (4,result_file2))
 
  p = subprocess.Popen('tcpdump -i eth3 -en icmp > %s' % result_file, shell=True)
  time.sleep(1)
  p = subprocess.Popen('nping --icmp --data-len 1350 --delay 0 -c 2 172.16.0.2', shell=True)
  time.sleep(delay)
  subprocess.Popen('killall tcpdump', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
  time.sleep(1)
  os.system('scp "root@%s:/root/%s" ./' % (hostname, result_file2) )

def packet_delay_test(result_file, result_file2, hostname, username, delay):
  tcpdump_cmd = "nohup ./call_tcpdump.sh %s %s"
  call_ssh(hostname, username, tcpdump_cmd % (2,result_file2))
  
  p = subprocess.Popen('tcpdump -i eth3 -en icmp > %s' % result_file, shell=True)
  time.sleep(1)
  p = subprocess.Popen('nping --icmp --data-len 1350 --delay 0 -c 1 172.16.0.2', shell=True)
  time.sleep(delay)
  subprocess.Popen('killall tcpdump', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
  time.sleep(1)
  os.system('scp "root@%s:/root/%s" ./' % (hostname, result_file2) )

def parse_file(result_file):
  requestes = []
  responses = []

  req_id = 0
  res_id = 0

  for line in  open(result_file, 'r'):
    #print line
    listtouse = requestes
    if 'request' in line:
      listtouse = requestes
    elif 'reply' in line:
      listtouse = responses
    else:
      continue

    ts = line.split(' ')[0]
    ts = int(ts.split('.')[0].split(':')[0])*24*60*60*1000000+int(ts.split('.')[0].split(':')[1])*60*60*1000000+int(ts.split('.')[0].split(':')[2])*60*1000000+int(ts.split('.')[1])
    seq = line.split(' ')[18].split(',')[0]
    entry = {'ts': ts, 'seq': seq}
    listtouse.append(entry)

  #print "Requests:", requestes
  #print "Responses", responses
  return requestes, responses

def read_delays(requestes, responses):
  results = []
  for idx, request in enumerate(requestes):
    #print idx, request
    if responses[int(idx)]['seq'] != request['seq']:
      print "Delays Error: unmatching seqs"
      break
    time = int(responses[int(idx)]['ts']) - int(request['ts'])
    results.append(time)
  
  #print "Times between request-response: ", results
  return results

def _calc_dispersion(array):
  array_disp = []
  for val in array:
    seq = val['seq']
    for val2 in array:
      if int(val['seq']) == int(val2['seq']) - 1:
        array_disp.append( int(val2['ts']) - int(val['ts']) )
  return array_disp

def read_dispersions_diff(requestes, responses):
  if len(responses) != len(requestes):
    print "ERROR: different number of requestes and responses!!"
  
  requestes_disp = _calc_dispersion(requestes)
  responses_disp = _calc_dispersion(responses)
  
  results = []
  if len(requestes_disp) != len(responses_disp):
    print "ERROR: different len for req and resp dispersions!!"
    return results

  for idx, request_disp in enumerate(requestes_disp):
    results.append(responses_disp[idx]) 
  
  return results


def calc_delay_values(result_file, result_file2):
  reqs,resps = parse_file(result_file)
  reqs2,resps2 = parse_file(result_file2)
  first_pkt_delay = read_delays(reqs,resps)
  first_pkt_oneway_delay = read_delays(reqs,reqs2)
  return first_pkt_delay,first_pkt_oneway_delay
  

def perform_tests(filename,filename2="onewaydelay.dump",hostname="10.15.254.148",username="root", delay=1):
  print "PREPARATION: Synch with NTP and wait for rules flush"
  os.system('ntpd')
  call_ssh(hostname,username,'ntpd')
  time.sleep(10)

  print "First packet delay test"
  packet_delay_test(filename, filename2, hostname, username, delay)
  first_pkt_delay, first_pkt_oneway_delay = calc_delay_values(filename,filename2)
  #time.sleep(1)
  print "Second packet delay test"
  packet_delay_test(filename, filename2, hostname, username, delay)
  second_pkt_delay, second_pkt_oneway_delay = calc_delay_values(filename,filename2)


  print "Waiting for rules flush"
  time.sleep(10)


  print "First packet pair test"
  pkt_pair_test(filename, filename2, hostname, username, delay) 
  reqs,resps = parse_file(filename)
  first_pp = read_dispersions_diff(reqs,resps)
  reqs2,resps2 = parse_file(filename2)
  first_pp_oneway = read_dispersions_diff(reqs2,reqs2)  

  time.sleep(1)
  print "Secpmd packet pair test"
  pkt_pair_test(filename, filename2, hostname, username, delay)
  reqs,resps = parse_file(filename)
  second_pp = read_dispersions_diff(reqs,resps)
  reqs2,resps2 = parse_file(filename2)
  second_pp_oneway = read_dispersions_diff(reqs2,reqs2)  


  print "============ RESULTS ============"
  print "1. Delay: first ", first_pkt_delay, " second ", second_pkt_delay
  print "1. ONEWAY first: %s second: " % first_pkt_oneway_delay 
  print "2. Dispersions ", first_pp, " second ", second_pp
  if len(first_pkt_delay) == 1 and len(second_pkt_delay) == 1 and len(first_pp) == 1 and len(second_pp) == 1:
    return first_pkt_delay[0],\
           first_pkt_oneway_delay[0],\
           second_pkt_delay[0],\
           second_pkt_oneway_delay[0],\
           first_pp[0],\
           first_pp_oneway[0],\
           second_pp[0],\
           second_pp_oneway[0]
  else:
    return 0,0,0,0,0,0,0,0


filename = "./icmp.pcap"
iterations = 99 #19


result = "# first-delay\t"
result += "first-delay oneway\t"
result += "second-delay\t"
result += "second-delay oneway\t"
result += "first-dispersion\t"
result += "first-dispersion oneway\t"
result += "second-dispersion\t"
result += "second-dispersion oneway\n"

for i in range(0,iterations,1):
  results = perform_tests(filename)
  result += "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t\n"%(results)
out_file = open("plain.dat","w")
out_file.write(str(result))
out_file.close()

subprocess.Popen('tc qdisc add dev eth1 root netem delay 10ms', shell=True)
result = "# first-delay\t"
result += "first-delay oneway\t"
result += "second-delay\t"
result += "second-delay oneway\t"
result += "first-dispersion\t"
result += "first-dispersion oneway\t"
result += "second-dispersion\t"
result += "second-dispersion oneway\n"
for i in range(0,iterations,1):
  results = perform_tests(filename)
  result += "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t\n"%(results)
out_file = open("controllink10ms.dat","w")
out_file.write(str(result))
out_file.close()
subprocess.Popen('tc qdisc del dev eth1 root netem delay 10ms', shell=True)

"""
subprocess.Popen('tc qdisc add dev eth3 root netem delay 20ms 4ms distribution pareto', shell=True)
result = "# first-delay\tsecond-delay\tfirst-dispersion\tsecond-dispersion\n"
for i in range(0,iterations,1):
  fde, sde, fdi, sdi = perform_tests(filename, delay=5)
  result += "%s\t%s\t%s\t%s\t\n"%(fde,sde,fdi,sdi)
out_file = open("pareto20-4.dat","w")
out_file.write(str(result))
out_file.close()
subprocess.Popen('tc qdisc del dev eth3 root netem delay 20ms 4ms distribution pareto', shell=True)
"""
