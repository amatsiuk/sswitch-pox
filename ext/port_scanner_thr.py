# -*- coding: utf-8 -*-
#!/usr/bin/env python
import socket
import subprocess
import sys
from datetime import datetime
import time
import threading

# Clear the screen
subprocess.call('clear', shell=True)

# Ask for input
remoteServer    = raw_input("Enter a remote host to scan: ")
remoteServerIP  = socket.gethostbyname(remoteServer)

num = raw_input("Enter number of ports to scan:")
num_ports  = int(num)

# Print a nice banner with information on which host we are about to scan
print "-" * 60
print "Please wait, scanning remote host", remoteServerIP
print "-" * 60

# Check what time the scan started
t1 = datetime.now()

# Using the range function to specify ports (here it will scans all ports between 1 and 1024)

# We also put in some error handling for catching errors

class WorkerThread(threading.Thread):

  def __init__(self, i, remoteServerIP):
    super(WorkerThread, self).__init__()
    self.port = i
    self.remoteServerIP = remoteServerIP

  def run(self):

    try:
          sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
          sock.settimeout(5)
          result = sock.connect_ex((self.remoteServerIP, self.port))
          if result == 0:
            print'*',
            #print "Port {}: \t Open".format(self.port)
          else:
            print '-',
            #print "Port {}: \t Closed".format(self.port)
          sock.close()
      #    self.join()

    except socket.gaierror:
        print 'Hostname could not be resolved. Exiting'
        sys.exit()

    except socket.error:
        print "Couldn't connect to server"
        sys.exit()

    except socket.timeout:
        print "0",


  def join(self, timeout=None):
    super(WorkerThread, self).join(timeout)

try:

  pool= [WorkerThread(i, remoteServerIP) for i in range(50000,50000+num_ports)]

  for thread in pool:
    thread.start()

except KeyboardInterrupt:
    print "You pressed Ctrl+C"
    sys.exit()

except socket.gaierror:
    print 'Hostname could not be resolved. Exiting'
    sys.exit()

except socket.error:
    print "Couldn't connect to server"
    sys.exit()


print "waitng threads..."
for thread in pool:
  thread.join()

# Checking the time again
t2 = datetime.now()

# Calculates the difference of time, to see how long it took to run the script
total =  t2 - t1

# Printing the information to screen
print 'Scanning Completed in: ', total